# binder.js

Lightweight two-way data binding in vanilla JS. Ditch Angular.

## Usage

Binder.js makes two-way data binding a cakewalk. Just add the data-binder="[id of your choice]" attribute to elements you wish to bind. 
Elements assigned the same id within the data-binder attribute will have their value and or inner html binded together, and will automatically update on the "keyup" event by default.
To initialize binder.js, simply assign a variable to binder: 


```javascript
var bind = binder();
```

You can also initialize data binding without using attributes within your elements if you prefer to have custom events:


```javascript
var bind = binder({id: 'hello_world', selector:'.hello-world', events: 'change click mousedown'});
```

The object passed to binder must include an id and a selector. If the events property is omitted, the default "keyup" event will be used.

To access a value from your binder variable:


```javascript
var value = bind.hello_world.value
```

To update a value in your binder variable (this will also update all bound elements) :


```javascript
bind.hello_world.update('Hello, World!');
```

To add an additional binded element to your binder variable:


```javascript
bind.hello_world.add(element);
```

NOTE: At this time, binder.js does not use jQuery. You must pass an element to this function. jQuery support will be added in the future.

If you would like to add an additional group of binded elements to the same binder variable:


```javascript
bind.init({id:'hello_world2', selector:'.hello-world2'});
```

NOTE: This will create a new group within your binder variable. This group can be accessed with its own id. It is possible to assign the same elements to multiple binder groups for advanced interactions.

### Future Features

* Ability to remove binded elements from a binder group
* Ability to add and remove events to a binder group
* Ability to pass a jQuery object to binder
* Additional data attributes for custom binding in your markup
* Conditional interactions between multiple binder groups



