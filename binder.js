!function() {

var binder = function binder (obj) {
	return new binder.fn.init(obj);
};

// binder group object
binder.group = function() {
	this.root   = null;
	this.value  = null;
	this.events = ['keyup'];
	this.depth  = 0;
}

// binder node object
binder.node = function(element){
	this.element 	= element;
	this.next 		= null;
}

// add a binder node to a group using a simple single linked list implementation
binder.group.prototype.add = function (element) {
	var node = null;
	if(!this.root) {
		this.root = new binder.node(element);
		this.depth++;
	} else {
		node = this.root;
		while(node) {
			if(!node.next) {
				node.next = new binder.node(element);
				this.depth++;
				break;
			} 
			node = node.next;
		}
	}
	return this;
}

// update binder group and associated nodes with a new value based on function input or defined events for the binder group
binder.group.prototype.update = function (e) {
	if(e.type && this.events.indexOf(e.type) >= 0) {
		this.value = e.currentTarget.value;
	} else {
		this.value = e;
	}
	var node = this.root;
	while(node) {
		node.element.innerHTML = this.value;
		node.element.value	   = this.value;
		node = node.next;
	}
}

// make the prototype pretty
binder.fn = binder.prototype = {};

binder.fn.init = function (obj) {
	var elements, i, ie, id, events;
	if(typeof obj === 'object') {
		if(obj.hasOwnProperty('id') && obj.hasOwnProperty('selector')) {
			id 		 = obj.id;
			elements = Array.prototype.slice.call(document.querySelectorAll(obj.selector));
			events   = obj.hasOwnProperty('events') ? obj.events : null;
		} else {
			throw new Error ('Invalid constructor object');
		}
	} else {
		elements = Array.prototype.slice.call(document.querySelectorAll('[data-binder]'));
		id 		 = null;
		events   = null;
	}
	i = elements.length;
	while(i--){
		id = id || elements[i].getAttribute('data-binder');
		if(!this.hasOwnProperty(id)) {
			this[id] = new binder.group();
			this[id].events = events || this[id].events;
		}
		ie = this[id].events.length;
		while(ie--) {
			elements[i].addEventListener(this[id].events[ie], this[id].update.bind(this[id]));
		}
		this[id].add(elements[i]);
	}
	return this;
}

binder.fn.init.prototype = binder.fn;

// load binder to the DOM window
if(typeof window.binder === 'undefined') {
	window.binder = binder;
} else {
	throw new Error ('Conflict assigning "binder" to global scope');
}

}();
